import { screen, render } from '@testing-library/react';
import App from "../App.jsx";
describe('App', () => {
  it('should render main title', () => {
    render(<App />);
    expect(screen.getByRole('heading', {level:1, name: 'Vite + React'})).toBeInTheDocument();
  });
});
